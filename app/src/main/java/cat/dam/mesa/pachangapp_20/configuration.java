package cat.dam.mesa.pachangapp_20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ToggleButton;
import android.app.Activity;

public class configuration extends Activity{
    private ToggleButton tb;
    private TextView suggeriments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        suggeriments = (TextView) findViewById(R.id.suggeriments);
        //inicialitzar el control
        tb = (ToggleButton) findViewById(R.id.toggleButton1);
        suggeriments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creem l'Intent
                Intent intent = new Intent(configuration.this, suggeriment.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("isLogin",false);
                configuration.this.startActivity(intent);
                configuration.this.finish();
            }
        });
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }
}