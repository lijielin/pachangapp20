package cat.dam.mesa.pachangapp_20;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Chat extends Activity {

    //**COMU
    private ImageView iv_esport, iv_mapa,iv_chat,iv_conf;
    //**

    //Firebase
    private DatabaseReference bd;
    private  String nom = setting.name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //**COMU
        iv_esport = findViewById(R.id.iv_esport);
        iv_mapa = findViewById(R.id.iv_mapa);
        iv_chat = findViewById(R.id.iv_chat);
        //**

        //**COMU
        iv_esport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Esport.class));
            }
        });
        iv_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Mapa.class));
            }
        });
        //**

        //Firebase

        bd = FirebaseDatabase.getInstance().getReference("Missatge");
        final TextView tv_chat = (TextView) findViewById(R.id.tv_chat);

        bd.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String [] Missatges = dataSnapshot.getValue().toString().split(",");

                tv_chat.setText(""); //Netejar l'area de text

                for(int i = 0; i<Missatges.length ;i++){

                    String[] finalM = Missatges[i].split("=");
                    //tv_chat.append(finalM[1] + "\n");
                    tv_chat.append(nom+" : "+finalM[1] + "\n");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                tv_chat.setText("Cancel·lat");
            }
        });
    }

    public void enviarMissatge(View v){
        final EditText et_chat = (EditText) findViewById(R.id.et_chat);

        bd.child(Long.toString(System.currentTimeMillis())).setValue(et_chat.getText().toString());
        et_chat.setText("");
    }
}
