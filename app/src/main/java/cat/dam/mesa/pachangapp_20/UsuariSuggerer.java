package cat.dam.mesa.pachangapp_20;

public class UsuariSuggerer {

    private String uid;
    private String nom;
    private String suggeriment;
    //getters i setters
    //constructors
    public UsuariSuggerer() {}

    public UsuariSuggerer(String uid, String nom,String suggeriment) {
        this.uid = uid;
        this.nom = nom;
        this.suggeriment = suggeriment;
    }
    public String getUid() {
        return uid;
    }
    public String getNom() {
        return nom;
    }
    public String getSuggeriment() {
        return suggeriment;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSuggeriment(String suggeriment) {
        this.suggeriment = suggeriment;
    }

    public void setUid(String uid) { this.uid = uid; }

    public String toString(){
//Important fer mètode .toString per alimentar correctament Spinner o ListView
//ja que és el mètode que es crida automàticament per representar els objectes.
        return (nom);
    }
}
